<!doctype html>

<html lang="ru">

  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
    <title>WEB</title>
  </head>

  <body class="p-0 m-0 body" >
  
    <header class="container-fluid justify-content-center logo header">
      <div class="container col-md-8"><img id="logo" src="image.png" alt="logo"><span>Название сайта</span></div>
    </header>
    </div>
	<div class="row mx-auto">
      <nav class="col-md-8 pb-1 pt-1 mx-auto">
        <div class="row justify-content-center text-center logo" id="menu">
          <a class="col-12 col-md-4" href="#hyper">Гиперссылки</a>
          <a class="col-12 col-md-4" href="#table">Таблица</a>
          <a class="col-12 col-md-4" href="#forms">Форма</a>
        </div>
      </nav>
      
      <article class="col-md-8 mx-auto artic">
        <h1 class="text-center">WEB</h1>
        <div class="row">
          <section class="col-12 col-md-12 mb-5" id="forms">
            <h2>Формы</h2>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $mess) {
	print($mess);
  }
  print('</div><br/><br/>');
}
?>
            <form action="" method="POST">
              <br/>Имя:<br/>
              <input type="text" name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"><br/>
              <br/>email:<br/>
              <input type="email" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br/>
              <br/>Дата рождения:<br/>
                <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="text"/>
              <br/>
                <br/><a id="gender"></a>Пол:<br/>
                  <input type="radio" name="gender" value="male" <?php if ($values['gender'] == 'male') {print 'checked="checked"';} ?>/>Мужской<br/>
                  <input type="radio" name="gender" value="female" <?php if ($values['gender'] == 'female') {print 'checked="checked"';} ?>/>Женский<br/>
              <br/>Кол-во конечностей:<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="1" <?php if ($values['limb'] == '1') {print 'checked="checked"';} ?> />1<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="2" <?php if ($values['limb'] == '2') {print 'checked="checked"';} ?> />2<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="3" <?php if ($values['limb'] == '3') {print 'checked="checked"';} ?> />3<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="4" <?php if ($values['limb'] == '4') {print 'checked="checked"';} ?> />4<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value=">4" <?php if ($values['limb'] == '>4') {print 'checked="checked"';} ?> />>4<br/>    
              <br/>Сверхспособности:<br/>
                <input type="checkbox" name="super1" value="бессмертие" <?php if ($values['super1'] != '') {print 'checked="checked"';} ?> />Бессмертие<br/>
                <input type="checkbox" name="super2" value="прохождение сквозь стены" <?php if ($values['super2'] != '') {print 'checked="checked"';} ?> />Прохождение сквозь стены<br/>
                <input type="checkbox" name="super3" value="левитация" <?php if ($values['super3'] != '') {print 'checked="checked"';} ?> />Левитация<br/>
                <input type="checkbox" name="super4" value="зачёт по дискре" <?php if ($values['super4'] != '') {print 'checked="checked"';} ?> />Зачёт по дискретке за один день<br/>
              <br/>Биография:<br/>
                <textarea name="message" value="<?php print $values['message']; ?>">Расскажите о себе</textarea>
                <input type="checkbox" name="check" value="+" <?php if ($values['check'] != '') {print 'checked="checked"';} ?>/>С контрактом ознакомлен<br/>
              <input type="submit" name="send" value="Отправить" class="submit"/>        
            </form>
          </section>
	      <section class="col-12 col-md-12" id="hyper">
            <h2>Гиперссылки</h2>
            <ol>
            <li>
			    <a href="http://html5book.ru">Абсолютная гиперссылка на главную страницу сайта html5book.ru</a>
			</li>
            <li>
		        <a href="https://example.com">Абсолютная гиперсылка на главгую страницу сайта Example.com в протоколе https</a> 
		    </li>
            <li>
		        <a href="ftp://example.com/test.txt">Ссылка на файл на сервере FTP без авторизации</a>
		    </li> 
            <li>
		        <a href="ftp://логин:пароль@files.000webhost.com/public_html/camaro.jpg" title="Camaro SS">Ссылка на файл на сервере FTP с авторизацией</a>
		    </li>
            <li>
		        <a href="https://ru.wikipedia.org/wiki/Википедия#firstHeading">Ссылка на фрагмент страницы некоторого сайта</a>
		    </li>
            <li>
		        <a href="#top">Ссылка на фрагмент текущей страницы(Переход в начало)</a>
		    </li> 
            <li>
		        <a href="https://translate.yandex.ru/?lang=en-ru&text=parser">Ссылка с двумя параметрами в URL</a>
		    </li>
            <li>
                <nav>
                    <ul>
                        <li>
                            <a href="https://www.kubsu.ru/ru/university" title="Университет">Университет</a>
                        </li>
                        <li>
                            <a href="https://www.kubsu.ru/ru/abiturient" title="Абитуриенту">Абитуриенту</a>
                        </li>
                    </ul>
                </nav>
            </li>
            <li>
                <a>Ссылка без href</a>
            </li>
            <li>
                <a href="">Ссылка с пустым href</a>
            </li>
            <li>
                <a rel="nofollow" href="https://peaky-blinders.fandom.com/ru/wiki/%D0%90%D1%80%D1%82%D1%83%D1%80_%D0%A8%D0%B5%D0%BB%D0%B1%D0%B8">Ссылка, по которой запрещен переход поисковикам</a>
            </li>
            <li>
                <!--<noindex>-->
                <a href="https://peaky-blinders.fandom.com/ru/wiki/%D0%A2%D0%BE%D0%BC%D0%B0%D1%81_%D0%A8%D0%B5%D0%BB%D0%B1%D0%B8">Ссылка запрещенная для индексации поисковиками</a>
                <!--</noindex>-->
            </li>
            <li>
                <p>Это азбзац внутри, которого <a href="https://seo-akademiya.com/seo-wiki/kontekstnaya-ssyilka/">контекстная ссылка</a>.</p>
            </li>
            <li>
			    <a href="https://my-fine.com/blog/43550958509/Eti-milyie-zhivotnyie-zastavyat-vas-rasplyitsya-v-ulyibke!?nr=1"><img src="https://mtdata.ru/u8/photoC921/20280254700-0/original.jpg" width="250" height="350" alt="ламочка"></a>
			</li> 

            <li>
			   <map name="Собачки"> 
                <area shape="circle" coords="350,200,100" href="https://ru.wikipedia.org/wiki/%D0%92%D0%B5%D0%BB%D1%8C%D1%88-%D0%BA%D0%BE%D1%80%D0%B3%D0%B8" alt="white" title="Вельш-корги" > 
                <area shape="rect" coords="50,65,250,300" href="https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D0%B1%D0%B8%D1%80%D1%81%D0%BA%D0%B8%D0%B9_%D1%85%D0%B0%D1%81%D0%BA%D0%B8" alt="gold" title="Хаски"> 
                <img src="https://avatars.mds.yandex.net/get-zen_doc/1856150/pub_5ce4f84bda7a8100b3ddc653_5ce4f88b1e1bf500b3eeafe3/scale_1200" width="500" height="350" alt="Дружба между собачками" usemap="#Собачки"> 
               </map> 
           </li>
            <li>
               <a href="index.html">Относительая ссылка на страницу в текущем каталоге</a>
            </li>
            <li>
               <a href="about">Относительная ссылка на страницу в каталоге about</a>
            </li>
            <li>
               <a href="../catalog">Относительная ссылка на страницу в каталоге уровнем выше текущего</a>
            </li>
            <li>
               <a href="../../catalog">Относительная ссылка на страницу в каталоге двумя уровнями выше</a>
            </li>
            <li>
               <a href="/">Сокращённая ссылка на главную</a>
            </li>
            <li>
               <a href="/about">Сокращённая ссылка на внутреннюю</a>
            </li>
       
            </ol>
          </section>
          <section class="col-12 col-md-12" id="table">
	        <h2>Таблица</h2>
	        <table class="table table-hover">
              <tr>
                <th>Первая колонка</th>
	      	  <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th>Первая колонка</th>
                <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th>Первая колонка</th>
                <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th>Первая колонка</th>
                <th>Вторая колонка</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th colspan="2">Первая+Вторая колонки</th>
                <th>Третья колонка</th>
              </tr>
              <tr>
                <th colspan="3">Первая+Вторая+Третья колонки</th>
              </tr>
            </table>
          </section>
        </div>
	  </article>
	</div>
    <footer class="container-fluid mt-1 justify-content-center footer">
        <div>(с) Глонти Екатерина 2020</div>
    </footer>
  </body>
</html>